from django.db import models

# Create your models here.
class Friend(models.Model):
      name = models.CharField(max_length=35)
      npm = models.IntegerField()
      DOB = models.DateField()

      # title = models.CharField(max_length = 35)
      # description = models.TextField()
      # last_modified = models.DateTimeField(auto_now_add = True)
      # img = models.ImageField(upload_to = "images/")

      def __str__(self):
            return self.name

      # def __str__(self) -> str:
      #     return super().__str__()