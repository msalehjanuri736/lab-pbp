from django.shortcuts import render
from datetime import datetime, date
from lab_1.views import calculate_age
from .models import Friend
from django.http.response import HttpResponseRedirect
from django.contrib.auth import authenticate, login
from .forms import FriendForm

# Create your views here.
mhs_name = 'M. Saleh Januri'
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2002, 1, 25)
npm = 2006464625

def login_url(request):
      username = request.POST['username']
      password = request.POST['password']
      user = authenticate(request, username=username, passowrd=password)
      if user is not None:
            login(request, user)
            return HttpResponseRedirect('/')
      else:
            return HttpResponseRedirect('/')

def index(request):
      response = {'name': mhs_name,
                  'age': calculate_age(birth_date.year),
                  'npm': npm}
      return render(request, 'lab3_index.html', response)

def calculate_age(birth_year):
      return curr_year - birth_year if birth_year <= curr_year else 0

def index(request):
      friends = Friend.objects.all()
      response = {'friends': friends}
      return render(request, 'lab3_index.html', response)

def add_friend(request):
      add = {}
      # print("tes")
      form = FriendForm(request.POST or None)
      # if form.is_valid():
      if (form.is_valid() and request.method == 'POST'):
            form.save()
            return HttpResponseRedirect('/lab-3')
      # else:
      #       return HttpResponseRedirect('/')
      
      # add['form'] = form
      return render(request, "lab3_form.html", add)