import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const appTitle = 'Form Login Aplikasi';

    return MaterialApp(
      title: appTitle,
      home: Scaffold(
        appBar: AppBar(
          title: const Text(appTitle),
        ),
        body: const MyCustomForm(),
      ),
    );
  }
}

// Membuat sebuah form
class MyCustomForm extends StatefulWidget {
  const MyCustomForm({Key? key}) : super(key: key);

  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}

class MyCustomFormState extends State<MyCustomForm> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment:
            CrossAxisAlignment.start, // untuk mengatur posisi button login nya
        children: [
          TextFormField(
            decoration: new InputDecoration(
              hintText: "ex: msalehj",
              labelText: "Username",
              icon: Icon(Icons.people),
              border: OutlineInputBorder(
                  borderRadius: new BorderRadius.circular(5.0)),
            ),
            validator: (value) {
              if (value == null || value.isEmpty) {
                return 'Nama tidak boleh kosong';
              }
              return null;
            },
          ),
          TextFormField(
            decoration: new InputDecoration(
              hintText: "ex: P455worD",
              labelText: "Password",
              icon: Icon(Icons.lock),
              border: OutlineInputBorder(
                  borderRadius: new BorderRadius.circular(5.0)),
            ),
            validator: (value) {
              if (value == null || value.isEmpty) {
                return 'Password harus di isi';
              }
              return null;
            },
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0),
            child: ElevatedButton(
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(content: Text('Data sedang di proses')),
                  );
                }
              },
              child: const Text('Login'),
            ),
          ),
        ],
      ),
    );
  }
}
