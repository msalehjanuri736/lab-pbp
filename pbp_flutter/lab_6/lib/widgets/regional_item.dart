import 'package:flutter/material.dart';

import '../screens/regional_meals_screen.dart';

class RegionalItem extends StatelessWidget {
  final String id;
  final String title;
  final Color color;

  RegionalItem(this.id, this.title, this.color);

  void selectRegional(BuildContext ctx) {
    Navigator.of(ctx).pushNamed(
      RegionalsMealsScreen.routeName,
      arguments: {
        'id': id,
        'title': title,
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => selectRegional(context),
      splashColor: Theme.of(context).primaryColor,
      borderRadius: BorderRadius.circular(15),
      child: Container(
        padding: const EdgeInsets.all(15),
        child: Text(
          title,
          style: Theme.of(context).textTheme.headline6,
        ),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              color.withOpacity(0.7),
              color,
            ],
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
          ),
          borderRadius: BorderRadius.circular(15),
        ),
      ),
    );
  }
}
