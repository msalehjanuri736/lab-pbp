import 'package:flutter/material.dart';

// import 'lib.dart';
import '../widgets/meal_item.dart';
import '../models/meal.dart';

class RegionalsMealsScreen extends StatefulWidget {
  static const routeName = '/regional-meals';

  final List<Meal> availableMeals;

  RegionalsMealsScreen(this.availableMeals);

  @override
  _RegionalMealsScreenState createState() => _RegionalMealsScreenState();
}

class _RegionalMealsScreenState extends State<RegionalsMealsScreen> {
  String regionalTitle;
  List<Meal> displayedMeals;
  var _loadedInitData = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    if (!_loadedInitData) {
      final routeArgs =
          ModalRoute.of(context).settings.arguments as Map<String, String>;
      regionalTitle = routeArgs['title'];
      final regionalId = routeArgs['id'];
      displayedMeals = widget.availableMeals.where((meal) {
        return meal.regionals.contains(regionalId);
      }).toList();
      _loadedInitData = true;
    }
    super.didChangeDependencies();
  }

  void _removeMeal(String mealId) {
    setState(() {
      displayedMeals.removeWhere((meal) => meal.id == mealId);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(regionalTitle),
      ),
      body: ListView.builder(
        itemBuilder: (ctx, index) {
          return MealItem(
            id: displayedMeals[index].id,
            title: displayedMeals[index].title,
            imageUrl: displayedMeals[index].imageUrl,
            duration: displayedMeals[index].duration,
            affordability: displayedMeals[index].affordability,
            complexity: displayedMeals[index].complexity,
          );
        },
        itemCount: displayedMeals.length,
      ),
    );
  }
}
