import 'package:flutter/material.dart';

class Regional {
  final String id;
  final String title;
  final Color color;

  const Regional({
    @required this.id,
    @required this.title,
    this.color = Colors.orange,
  });
}
