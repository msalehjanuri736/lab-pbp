import 'package:flutter/material.dart';

void main() {
  runApp(new MaterialApp(
    title: "Lab_6",
    home: new Myapp(),
  ));
}

class Myapp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: new Text("Silahkan login terlebih dahulu"),
      ),
      body: new Container(
        padding: EdgeInsets.all(20.0),
        child: Column(
          children: [
//             TextField()
            TextFormField(
              decoration: new InputDecoration(
                hintText: "Nama Lengkap",
//                 labelText: "Nama Lengkap",
                icon: Icon(Icons.people),
                border: OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(5.0)),
              ),
              validator: (value) {
                if (value == null) {
                  return 'Nama tidak boleh kosong';
                }
                return null;
              },
            ),
            TextFormField(
                decoration: new InputDecoration(
              hintText: "Password",
//                 labelText: "Nama Lengkap",
              icon: Icon(Icons.lock),
              border: OutlineInputBorder(
                  borderRadius: new BorderRadius.circular(5.0)),
            )),
            RaisedButton(
              child: Text(
                "Login",
                style: TextStyle(color: Colors.white),
              ),
              color: Colors.blue,
              onPressed: () {
//                 if (_formKey.currentState.validate()) {}
              },
            ),
          ],
        ),
      ),
    );
  }
}
