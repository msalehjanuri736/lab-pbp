from django.urls import path
from .views import index, lab2, xml, json


urlpatterns = [
      path('', index, name='index'),
      path('lab2', lab2, name='lab2'),
      path('xml', xml, name='xml'),
      path('json', json, name='json')
]