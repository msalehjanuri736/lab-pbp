from django.shortcuts import render
from django.http.response import HttpResponse
from django.core import serializers
from .models import Note

# Create your views here.
note = 'My Biodata'
To = ''
From = ''
Title = ''
Message = ''

def index(request):
      response = {'note': note,
                  'To': To,
                  'from': From,
                  'Title': Title,
                  'message': Message}
      return render(request, 'index_lab2.html', response)

def lab2(request):
      notes = Note.objects.all()
      response = {'notes': notes}
      return render(request, 'lab2.html', response)

# Method untuk merender tampilan format XML
def xml(request):
      data = serializers.serialize('xml', Note.objects.all())
      return HttpResponse(data, content_type="application/xml")

# Method untuk merender tampilan format json
def json(request):
      data = serializers.serialize('json', Note.objects.all())
      return HttpResponse(data, content_type="application/json")

