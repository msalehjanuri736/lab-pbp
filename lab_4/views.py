from django.shortcuts import render
from .forms import NoteForm
from django.http import HttpResponseRedirect
from .models import Note

# Create your views here.
note = 'My Biodata'
To = ''
From = ''
Title = ''
Message = ''


def index(request):
      notes = Note.objects.all()
      response = {'notes': notes}
      return render(request, 'lab4_index.html', response)

def add_note(request):
      add = {}
      form = NoteForm(request.POST or None)
      if (form.is_valid() and request.method == 'POST'):
            form.save()
            return HttpResponseRedirect('/lab-4')
      return render(request, "lab4_form.html", add)

def note_list(request):
      forms = Note.objects.all()
      response = {'forms': forms}
      return render(request, 'lab4_note_list.html', response)
